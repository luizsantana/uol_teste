//
//  Beer.h
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Beer : NSObject

@property (nonatomic, strong) NSString *idBeer;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *tagLine;
@property (nonatomic, strong) NSNumber *abv;
@property (nonatomic, strong) NSNumber *ibu;
@property (nonatomic, strong) NSString *descriptionBeer;
@property (nonatomic, strong) NSString *urlPhoto;

@end
