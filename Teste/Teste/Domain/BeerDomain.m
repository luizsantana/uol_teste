//
//  BeerDomain.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import "BeerDomain.h"

#import "Beer.h"
#import "BeerProxy.h"

@implementation BeerDomain

#pragma mark getInstance

+(instancetype) getInstance {
    
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id sharedInstance = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        
        sharedInstance = [[BeerDomain alloc] init];
    });
    
    // returns the same object each time
    return sharedInstance;
}

#pragma mark Methods

-(void) getBeersForPage:(NSNumber *) page success:(void (^)(NSMutableArray *list)) _success failure:(void (^)(NSError *error)) _failure {
    
    [[BeerProxy getInstance] getBeersForPage:page success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSMutableArray *beerList = [NSMutableArray array];
        
        if (responseObject) {
            
            for (NSDictionary *beerItem in responseObject) {
                
                Beer *beer = [self parseBeer:beerItem];
                
                [beerList addObject:beer];
            }
        }
        
        _success(beerList);
        
    } failure:^(NSURLSessionTask *task, NSError *error) {
        
        _failure(error);
    }];
}

-(void) getBeerById:(NSNumber *) idBeer success:(void (^)(Beer *beer)) _success failure:(void (^)(NSError *error)) _failure {
    
    [[BeerProxy getInstance] getBeerById:idBeer success:^(NSURLSessionDataTask *task, id responseObject) {
        
        Beer *beer;
        
        if (responseObject) {
            
            for (NSDictionary *beerItem in responseObject) {
                
                beer = [self parseBeer:beerItem];
            }
        }
        
        _success(beer);
        
    } failure:^(NSURLSessionTask *task, NSError *error) {
        
        _failure(error);
    }];
}

#pragma mark Parse

-(Beer *) parseBeer:(NSDictionary *) item {
    
    Beer *beer = [Beer new];
    
    if ([item objectForKey:@"id"]) {
        
        beer.idBeer = [item objectForKey:@"id"];
    }

    if ([item objectForKey:@"name"]) {
        
        beer.name = [item objectForKey:@"name"];
    }

    if ([item objectForKey:@"tagline"]) {
        
        beer.tagLine = [item objectForKey:@"tagline"];
    }

    if ([item objectForKey:@"description"]) {
        
        beer.descriptionBeer = [item objectForKey:@"description"];
    }

    if ([item objectForKey:@"image_url"]) {
        
        beer.urlPhoto = [item objectForKey:@"image_url"];
    }

    if ([item objectForKey:@"abv"]) {
        
        beer.abv = [item objectForKey:@"abv"];
    }

    if ([item objectForKey:@"ibu"]) {
        
        beer.ibu = [item objectForKey:@"ibu"];
    }
    
    return beer;
}

@end
