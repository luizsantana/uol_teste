//
//  BeerDomain.h
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Beer;

@interface BeerDomain : NSObject

+(instancetype) getInstance;

-(void) getBeersForPage:(NSNumber *) page success:(void (^)(NSMutableArray *list)) _success failure:(void (^)(NSError *error)) _failure;
-(void) getBeerById:(NSNumber *) idBeer success:(void (^)(Beer *beer)) _success failure:(void (^)(NSError *error)) _failure;

@end
