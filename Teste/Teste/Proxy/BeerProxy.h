//
//  BeerProxy.h
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeerProxy : NSObject

+(instancetype) getInstance;

-(void) getBeersForPage:(NSNumber *) page success:(void (^)(NSURLSessionDataTask *task, id responseObject)) _success failure:(void (^)(NSURLSessionTask *task, NSError *error)) _failure;
-(void) getBeerById:(NSNumber *) idBeer success:(void (^)(NSURLSessionDataTask *task, id responseObject)) _success failure:(void (^)(NSURLSessionTask *task, NSError *error)) _failure;

@end
