//
//  BeerProxy.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import "BeerProxy.h"

#import <AFNetworking/AFNetworking.h>

@implementation BeerProxy

#pragma mark getInstance

+(instancetype) getInstance {
    
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id sharedInstance = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        
        sharedInstance = [[BeerProxy alloc] init];
    });
    
    // returns the same object each time
    return sharedInstance;
}

#pragma mark Request

-(void) requestPath:(NSString *) path mode:(NSString *) mode headers:(NSDictionary *) headers parameters:(NSDictionary *) parameters success:(void (^)(NSURLSessionDataTask *task, id responseObject)) _success failure:(void (^)(NSURLSessionTask * _Nonnull, NSError *error)) _failure {
    
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    if ([NSURLSessionConfiguration instancesRespondToSelector:@selector(ephemeralSessionConfiguration)]) {
        
        conf = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    }
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:conf];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSURL *url = [NSURL URLWithString:[URL_BASE stringByAppendingString:path]];
    
    if (headers) {
        
        for(NSString *key in headers) {
            
            [manager.requestSerializer setValue:[headers objectForKey:key] forHTTPHeaderField:key];
        }
    }
    
    if ([[mode uppercaseString] isEqualToString:@"DELETE"]) {
        
        [manager DELETE:url.absoluteString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            _success(task, responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            _failure(task, error);
        }];
    }
    else if ([[mode uppercaseString] isEqualToString:@"GET"]) {
        
        [manager GET:url.absoluteString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            _success(task, responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            _failure(task, error);
        }];
    }
    else if ([[mode uppercaseString] isEqualToString:@"POST"]) {
        
        [manager POST:url.absoluteString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            _success(task, responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            _failure(task, error);
        }];
    }
    else if ([[mode uppercaseString] isEqualToString:@"PUT"]) {
        
        [manager PUT:url.absoluteString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            _success(task, responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            _failure(task, error);
        }];
    }
}

#pragma mark Methods

-(void) getBeersForPage:(NSNumber *) page success:(void (^)(NSURLSessionDataTask *task, id responseObject)) _success failure:(void (^)(NSURLSessionTask *task, NSError *error)) _failure {
    
    [self requestPath:[NSString stringWithFormat:@"beers?page=%i&per_page=80", [page intValue]]  mode:@"GET" headers:nil parameters:nil success:_success failure:_failure];
}

-(void) getBeerById:(NSNumber *) idBeer success:(void (^)(NSURLSessionDataTask *task, id responseObject)) _success failure:(void (^)(NSURLSessionTask *task, NSError *error)) _failure {
    
    [self requestPath:[NSString stringWithFormat:@"beers/%i", [idBeer intValue]]  mode:@"GET" headers:nil parameters:nil success:_success failure:_failure];
}

@end
