//
//  BeersListViewController.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import "BeersListViewController.h"

#import "Beer.h"
#import "BeerDomain.h"

#import "BeerTableViewCell.h"
#import "BeerDetailViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface BeersListViewController ()

@property (nonatomic, strong) NSMutableArray *beersList;

@end

@implementation BeersListViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[BeerDomain getInstance] getBeersForPage:@1 success:^(NSMutableArray *list) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        self.beersList = list;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                                 message:[error domain]
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:nil];
        
        
        [alertController addAction: ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (!self.beersList) {
        
        return 0;
    }
    
    return [self.beersList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    BeerTableViewCell *beerCell = [tableView dequeueReusableCellWithIdentifier:@"BeerCell"];
    
    Beer *beer = [self.beersList objectAtIndex:indexPath.row];
    
    [beerCell setup:beer];
    
    return beerCell;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BeerDetailViewController *beerDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"BeerDetail"];
    
    Beer *beer = [self.beersList objectAtIndex:indexPath.row];
    [beerDetail setBeer:beer];
    
    [self.navigationController pushViewController:beerDetail animated:YES];
}

@end
