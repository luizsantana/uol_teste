//
//  BeerDetailViewController.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import "BeerDetailViewController.h"

#import "Beer.h"

#import "UIImageView+Haneke.h"

@interface BeerDetailViewController ()

@property (nonatomic, strong) IBOutlet UIImageView *ivPhoto;
@property (nonatomic, strong) IBOutlet UILabel *lbName;
@property (nonatomic, strong) IBOutlet UILabel *lbTagLine;
@property (nonatomic, strong) IBOutlet UILabel *lbDescription;
@property (nonatomic, strong) IBOutlet UILabel *lbAbv;
@property (nonatomic, strong) IBOutlet UILabel *lbIbu;

@property (nonatomic, strong) Beer *beerDetail;

@end

@implementation BeerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];

    [self setup:self.beerDetail];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setBeer:(Beer *) beer {
    
    self.beerDetail = beer;
}

-(void) setup:(Beer *) beer {
    
    self.ivPhoto.image = nil;
    [self.ivPhoto hnk_setImageFromURL:[NSURL URLWithString:beer.urlPhoto]];
    
    self.lbName.text = beer.name;
    
    self.lbTagLine.text = beer.tagLine;
    
    self.lbAbv.text = [NSString stringWithFormat:@"Teor alcoólico: %i%%", [beer.abv intValue]];
    
    if ([beer.ibu isKindOfClass:[NSNull class]]) {
        
        self.lbIbu.text = @"Não disponível";
    }
    else {
        
        self.lbIbu.text = [NSString stringWithFormat:@"Escala de amargor: %i IBU", [beer.ibu intValue]];
    }
    
    self.lbDescription.text = beer.descriptionBeer;
}

@end
