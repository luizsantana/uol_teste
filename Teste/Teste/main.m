//
//  main.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 24/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
