//
//  BeerTableViewCell.h
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Beer;

@interface BeerTableViewCell : UITableViewCell

-(void) setup:(Beer *) beer;

@end
