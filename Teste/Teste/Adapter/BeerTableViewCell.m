//
//  BeerTableViewCell.m
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 25/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import "BeerTableViewCell.h"

#import "Beer.h"

#import "UIImageView+Haneke.h"

@interface BeerTableViewCell ()

@property (nonatomic, strong) IBOutlet UIImageView *ivPhoto;
@property (nonatomic, strong) IBOutlet UILabel *lbName;
@property (nonatomic, strong) IBOutlet UILabel *lbAbv;

@end

@implementation BeerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse {
    
    if (self.ivPhoto) {
        
        [self.ivPhoto hnk_cancelSetImage];
        self.ivPhoto.image = nil;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setup:(Beer *) beer {

    self.ivPhoto.image = nil;
    [self.ivPhoto hnk_setImageFromURL:[NSURL URLWithString:beer.urlPhoto]];
    
    self.lbName.text = beer.name;
    
    self.lbAbv.text = [NSString stringWithFormat:@"Teor alcoólico: %i%%", [beer.abv intValue]];
}

@end
