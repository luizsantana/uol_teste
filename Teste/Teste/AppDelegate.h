//
//  AppDelegate.h
//  Teste
//
//  Created by Luiz Carlos Sant'Ana Junior on 24/5/17.
//  Copyright © 2017 TourGuy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

